* For test application:

Generate a report in Spanish:
    http://localhost:8080/es_ES/jose%20luis

Generate a report in English:
    http://localhost:8080/en_EN/jose%20luis

* Resources:<br/>

    resources/template -> Fonts of JasperReport templates<br/>
    resources/compile -> JasperReport templates<br/>
    resources/i18n -> Tests in multilanguage<br/>