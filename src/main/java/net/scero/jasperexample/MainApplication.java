package net.scero.jasperexample;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/*
 *
 *
 * @author joseluis.nogueira on 18/02/2019
 */
@SpringBootApplication
@EnableWebMvc
@ComponentScan
@Slf4j
public class MainApplication { //NOSONAR
  /**
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) {
    try {
      SpringApplication springApplication = new SpringApplication(MainApplication.class);
      springApplication.run(args);
    } catch (Exception ex) {
      log.error("Error: " + ex);
      throw ex;
    }
  }
}

