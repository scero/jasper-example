package net.scero.jasperexample.controller;

import lombok.extern.slf4j.Slf4j;
import net.scero.jasperexample.dto.ExternalValuesDTO;
import net.scero.jasperexample.service.CreateReport;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Locale;

/*
 *
 *
 * @author joseluis.nogueira on 19/02/2019
 */
@RestController
@Slf4j
public class TestController {
  @Autowired
  private CreateReport createReport;

  @RequestMapping("/{locale}/{name}")
  public String message(@PathVariable String name, @PathVariable String locale) {
    if (!locale.equals("es_ES") && !locale.equals("en_EN")){
      return "Unknown locale";
    }
    ExternalValuesDTO externalValuesDTO = new ExternalValuesDTO();
    externalValuesDTO.setName(name);

    String path;
    try {
      path = createReport.create(externalValuesDTO, new Locale(locale));
    } catch (JRException | IOException e) {
      log.error("Error generating report", e);
      return "Error generating report";
    }
    return "Create report: " + path;
  }
}
