package net.scero.jasperexample.dto;

import lombok.Data;

/*
 *
 *
 * @author joseluis.nogueira on 18/02/2019
 */
@Data
public class ExternalValuesDTO {
  private String name;
}
