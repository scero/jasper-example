package net.scero.jasperexample.service;

import net.scero.jasperexample.dto.ExternalValuesDTO;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.*;

/*
 *
 *
 * @author joseluis.nogueira on 18/02/2019
 */
@Component
@Slf4j
public class CreateReport {
  private static final String JASPER_TEMPLATE = "jasper/compileTemplate/reportExample.jasper";
  private static final String BUNDLE_EXAMPLE = "jasper/i18n/reportExample";
  private static final String OUTPUT_FILE = "/tmp/reportExample.PDF";

  public String create(ExternalValuesDTO externalValuesDTO, Locale locale) throws JRException, IOException {
    // Map parameters = new HashMap();
    Map parameters = new HashMap<>();
    parameters.put(JRParameter.REPORT_LOCALE, locale);
    parameters.put("TITLE", externalValuesDTO.getName());

    JasperReport jasperReport = (JasperReport) JRLoader.loadObject(getClass().getClassLoader().getResource(JASPER_TEMPLATE));

    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
    JasperExportManager.exportReportToPdfFile(jasperPrint, OUTPUT_FILE);
    return new File(OUTPUT_FILE).getAbsolutePath();
  }
}
